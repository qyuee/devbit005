package com.cafe24.social.oauth2.cafe24.api;

public interface ShopOperations {
	
	/**
	 * 해당 access_token의 발급자의 shop 정보를 탐색.
	 * @return
	 */
	Shop getShopInfo();
	
	Shop getShopNo();
	
	Shop getShopName();
	
	Shop getMallId();
	
	Shop getPresidentName();
	
	Shop getMallUrl();
}
