package com.cafe24.social.oauth2.cafe24;

import org.springframework.social.oauth2.AccessGrant;

@SuppressWarnings("serial")
public class Cafe24AccessGrant extends AccessGrant{
	
	private String mallId;
	
	private String userId;
	
	private Long issuedAt;
	
	public Cafe24AccessGrant(String accessToken) {
		this(accessToken, null, null, null, null, null ,null);
	}
	
	public Cafe24AccessGrant(
			String accessToken,
			String scope,
			String refreshToken,
			Long expiresIn,
			String mallId,
			String userId,
			Long issuedAt
			) {
		
		super(accessToken, scope, refreshToken, expiresIn);
		
		this.mallId = mallId;
		this.userId = userId;
		this.issuedAt = issuedAt;
		
	}

	@Override
	public String getAccessToken() {
		return super.getAccessToken();
	}

	@Override
	public String getScope() {
		return super.getScope();
	}

	@Override
	public String getRefreshToken() {
		return super.getRefreshToken();
	}

	@Override
	public Long getExpireTime() {
		return super.getExpireTime();
	}

	public String getMallId() {
		return mallId;
	}

	public String getUserId() {
		return userId;
	}

	public Long getIssuedAt() {
		return issuedAt;
	}
	
}
