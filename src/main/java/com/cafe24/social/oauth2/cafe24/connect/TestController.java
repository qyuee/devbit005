package com.cafe24.social.oauth2.cafe24.connect;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.connect.web.ConnectSupport;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class TestController {

	@Autowired
	private ConnectSupport connectsupport;

	@Autowired
	private ConnectionFactoryLocator connectionFactoryLocator;

	@Autowired
	private UsersConnectionRepository JPAConnectionRepository;

	@RequestMapping(value = "/getCodeWithSocial", method = RequestMethod.GET)
	public RedirectView cafe24_authorization_code(NativeWebRequest request) {
		ConnectionFactory<?> connectionFactory = connectionFactoryLocator.getConnectionFactory("cafe24");
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("scope", "mall.read_category, mall.read_product, mall.read_store");
		
		String authorizeUrl = connectsupport.buildOAuthUrl(connectionFactory, request, parameters);
		System.out.println("authorizeUrl:" + authorizeUrl);
		return new RedirectView(authorizeUrl);
	}
	
	// provider가 redirectUrl를 통해 code를 파라미터에 붙여서 다시 되돌아 올 때.
	@RequestMapping(value = "/getCodeWithSocial", method = RequestMethod.GET, params="code")
	public RedirectView oauth2Callback(
			NativeWebRequest request,
			Model model) {
		OAuth2ConnectionFactory<?> connectionFactory = (OAuth2ConnectionFactory<?>) connectionFactoryLocator.getConnectionFactory("cafe24");
		
		String code = request.getParameter("code");
		
		AccessGrant accessGrant = connectionFactory.getOAuthOperations().exchangeForAccess(code, "https://lee33397.cafe24.com/devbit005/oauth/callback", null);
		
		System.out.println("======================Access Grant 관련 로그==================");
		System.out.println("accessGrant.getAccessToken() : "+accessGrant.getAccessToken());
		System.out.println("accessGrant.getRefreshToken() : "+accessGrant.getRefreshToken());
		System.out.println("accessGrant.getExpireTime() : "+accessGrant.getExpireTime());
		System.out.println("accessGrant.getScope() : "+accessGrant.getScope());
		System.out.println(""+accessGrant);
		System.out.println("===========================================================");
		
		
		Connection<?> connection = connectionFactory.createConnection(accessGrant);
		
		//Connection<?> connection = connectsupport.completeConnection(connectionFactory, request);
		
		System.out.println("\n\n===================connection 관련 로그===================");
		System.out.println("Connection : "+connection);
		System.out.println("connection.createData().getProviderUserId() : "+connection.createData().getProviderUserId());
		System.out.println("connection.getKey().getProviderUserId() : "+connection.getKey().getProviderUserId());
		System.out.println("connection.getKey().getProviderId() : "+connection.getKey().getProviderId());
		System.out.println(connection);
		System.out.println("=======================================================\n\n");
		
		// error : UserId is can not be null 
		ConnectionRepository connectionRepository = JPAConnectionRepository.createConnectionRepository("qyuee");
		
		connectionRepository.addConnection(connection);
		
		return connectionStatusRedirect("cafe24", request);
	}
	
	protected RedirectView connectionStatusRedirect(String providerId, NativeWebRequest request) {
		HttpServletRequest servletRequest = request.getNativeRequest(HttpServletRequest.class);
		String path = "https://lee33397.cafe24.com/devbit005/oauth/callback";
		return new RedirectView(path, true);
	}
	
	

}
