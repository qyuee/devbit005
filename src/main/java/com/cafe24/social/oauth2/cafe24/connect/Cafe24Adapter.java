package com.cafe24.social.oauth2.cafe24.connect;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

import com.cafe24.social.oauth2.cafe24.api.Cafe24;
import com.cafe24.social.oauth2.cafe24.api.Shop;

public class Cafe24Adapter implements ApiAdapter<Cafe24>{

	@Override
	public boolean test(Cafe24 cafe24) {
		return false;
	}

	@Override
	public void setConnectionValues(Cafe24 cafe24, ConnectionValues values) {
		
		Shop shopinfo = cafe24.shopOperations().getShopInfo();
		
		System.out.println(" >>> shopinfo : "+shopinfo);
		
		values.setProviderUserId(shopinfo.getMallId());
		values.setDisplayName(shopinfo.getPresidentName());
		values.setProfileUrl("");
		values.setImageUrl(shopinfo.getMallUrl());
	}

	@Override
	public UserProfile fetchUserProfile(Cafe24 cafe24) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateStatus(Cafe24 cafe24, String message) {
		// TODO Auto-generated method stub
		
	}

}
