package com.cafe24.social.oauth2.cafe24.connect;

import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;

import com.cafe24.social.oauth2.cafe24.api.Cafe24;

public class Cafe24ConnectionFactory extends OAuth2ConnectionFactory<Cafe24>{
	
	public Cafe24ConnectionFactory(String appId, String appSecret) {
		super("cafe24", new Cafe24ServiceProvider(appId, appSecret), new Cafe24Adapter());
	}

	// 의문점 과연 ProviderUserId는 무엇 일까?
	@Override
	protected String extractProviderUserId(AccessGrant accessGrant) {
		
		System.out.println(" >>> Cafe24ConnectionFactory's extractProviderUserId is called!! ");
		
		//return accessGrant;
		return "hello";
	}
	
}
