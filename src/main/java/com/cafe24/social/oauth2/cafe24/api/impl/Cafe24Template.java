package com.cafe24.social.oauth2.cafe24.api.impl;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.support.ClientHttpRequestFactorySelector;
import org.springframework.social.support.HttpRequestDecorator;
import org.springframework.web.client.RestTemplate;

import com.cafe24.social.oauth2.cafe24.api.Cafe24;
import com.cafe24.social.oauth2.cafe24.api.ShopOperations;

public class Cafe24Template extends AbstractOAuth2ApiBinding implements Cafe24{
	
	private RestTemplate usingApiRestTemplate;
	private String accessToken;
	
	private ShopOperations shopOperations;
	
	@Override
	public ShopOperations shopOperations() {
		return shopOperations;
	}
	
	public Cafe24Template(String accessToken) {
		super(accessToken);
		this.accessToken = accessToken;
		initialize();
	}
	
	private void initialize() {
		// Wrap the request factory with a BufferingClientHttpRequestFactory so that the error handler can do repeat reads on the response.getBody()
		super.setRequestFactory(ClientHttpRequestFactorySelector.bufferRequests(getRestTemplate().getRequestFactory()));
		
		usingApiRestTemplate = new RestTemplate(ClientHttpRequestFactorySelector.getRequestFactory());
		usingApiRestTemplate.setInterceptors(Arrays.asList(new ClientHttpRequestInterceptor[]{new Cafe24ApiHeaderBearerOAuth2RequestInterceptor(accessToken)}));
		
		initSubApis();
	}
	
	private void initSubApis() {
		shopOperations = new ShopTemplate(getRestTemplate(), usingApiRestTemplate);
	}
	
	/**
	 * 
	 * Cafe24 api를 사용할 때 Authorization header를 추가하는 인터셉터 class
	 * 
	 * @author bit
	 *
	 */
	class Cafe24ApiHeaderBearerOAuth2RequestInterceptor implements ClientHttpRequestInterceptor{
		
		private String accessToken;
		
		public Cafe24ApiHeaderBearerOAuth2RequestInterceptor(String accessToken) {
			this.accessToken = accessToken;
		}
		
		@Override
		public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
				throws IOException {
			
			HttpRequest protectedResourceRequest = new HttpRequestDecorator(request);
			protectedResourceRequest.getHeaders().set("Authorization", "Bearer " + accessToken);
			return execution.execute(protectedResourceRequest, body);
		}
	}

}
