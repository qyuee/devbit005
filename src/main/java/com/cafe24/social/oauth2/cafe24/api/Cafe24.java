package com.cafe24.social.oauth2.cafe24.api;

import org.springframework.social.ApiBinding;

public interface Cafe24 extends ApiBinding{
	
	/**
	 * Store 정보 API 호출
	 * @return 
	 */
	ShopOperations shopOperations();
}
