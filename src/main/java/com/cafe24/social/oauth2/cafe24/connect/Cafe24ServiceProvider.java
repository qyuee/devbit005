package com.cafe24.social.oauth2.cafe24.connect;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Template;

import com.cafe24.social.oauth2.cafe24.Cafe24OAuth2Template;
import com.cafe24.social.oauth2.cafe24.api.Cafe24;
import com.cafe24.social.oauth2.cafe24.api.impl.Cafe24Template;

public class Cafe24ServiceProvider extends AbstractOAuth2ServiceProvider<Cafe24>{
	
	private final static String authorizeUrl = "https://qyuee.cafe24api.com/api/v2/oauth/authorize";
	
	private final static String accessTokenUrl = "https://qyuee.cafe24api.com/api/v2/oauth/token";
	
	public Cafe24ServiceProvider(String appId, String appSecret) {
		super(getOAuth2Template(appId, appSecret));
	}
	
	private static OAuth2Operations getOAuth2Template(String appId, String appSecret) {
		OAuth2Template oAuth2Template = new Cafe24OAuth2Template(appId, appSecret, authorizeUrl, accessTokenUrl);
		
		// boolean 기본값이 false 이기에 Daum의 예제는 동작하는 것.
		// 이 부분이 true면 appid와 client를 파라미터로 사용해서 인증절차를 한다.
		// false면 파라미터가 아닌 header에 추가하여 인증 절차를 진행한다.
		oAuth2Template.setUseParametersForClientAuthentication(false);
		
		return oAuth2Template;
	}
	

	@Override
	public Cafe24 getApi(String accessToken) {
		return new Cafe24Template(accessToken);
	}
	
}
