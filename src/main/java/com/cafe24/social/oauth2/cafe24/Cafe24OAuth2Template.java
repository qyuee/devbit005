package com.cafe24.social.oauth2.cafe24;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.OAuth2Template;

public class Cafe24OAuth2Template extends OAuth2Template{

	public Cafe24OAuth2Template(String clientId, String clientSecret, String authorizeUrl, String accessTokenUrl) {
		super(clientId, clientSecret, authorizeUrl, accessTokenUrl);
		System.out.println(" >> Cafe24OAuth2Template is Created!! ");
	}

	/*
	 * Cafe24의 Token Response중 scope와 expires_In이 각각 scopes(배열), expires_at으로 key 값이 다르다.
	 * 그렇기에 null이 나온다.
	 * 
	 * 이를 OAuth2Template의 createAccessGrant메소드를 오버라이드해서 필요에 맞도록 재정의 해줘야 한다.
	 * 
	 */
	@Override
	protected AccessGrant createAccessGrant(String accessToken, String scope, String refreshToken, Long expiresIn,
			Map<String, Object> response) {
		
		System.out.println(" >> Cafe24OAuth2Template.createAccessGrant is called!! ");
		
		if(response.get("expires_in")==null) {
			expiresIn=getExpiresTime(response, "expires_at", "issued_at");
		}
		
		if(response.get("scope")==null) {
			scope=getScopesToString(response, "scopes");
		}
		
		return super.createAccessGrant(accessToken, scope, refreshToken, expiresIn, response);
	}
	
	
	/**
	 * 
	 * @param map
	 * @param key1 - value of expired_at
	 * @param key2 - value of issued_at
	 * @return vaildation millisecond time(유효시간, ms)
	 */
	private Long getExpiresTime(Map<String, Object> map, String key1, String key2) {
		try {
			Long expired_at = changeStringToMillis((String)map.get(key1));
			
			Long issued_at = changeStringToMillis((String)map.get(key2));
			
			System.out.println(" >> expired_at : "+expired_at);
			System.out.println(" >> issued_at : "+issued_at);
			
			System.out.println(" >> expired_at - issued_at : "+(expired_at-issued_at));
			
			return (expired_at-issued_at)/1000L;
		} catch (NumberFormatException | ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @param timeOfString - 2018-06-14T14:36:45.110 형태의 문자열
	 * 
	 * 2018-06-14 14:35:45.110 -> millisecond 형태로
	 * 
	 * @return The millisecond convert of timeOfString
	 * @throws ParseException
	 * 
	 */
	private Long changeStringToMillis(String timeOfString) throws ParseException {
		
		timeOfString = timeOfString.replace('T', ' ');
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		Date date = format.parse(timeOfString);
		
		return date.getTime();
	}
	
	private String getScopesToString(Map<String, Object> map, String key) {
		
		ArrayList<String> scopes = (ArrayList<String>) map.get("scopes");
		
		String scope = String.join(", ", scopes);
		
		System.out.println(" >> scope : "+scope);
		
		return scope;
	}
	
}
