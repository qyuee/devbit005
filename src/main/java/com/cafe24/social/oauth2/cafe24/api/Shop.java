package com.cafe24.social.oauth2.cafe24.api;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Shop extends Cafe24Object implements Serializable{
	
	private int shopNo;
	
	private String shopName;
	
	private String mallId;
	
	private String presidentName;
	
	private String mallUrl;
	
	public Shop() {
		
	}
	
	public Shop(int shopNo, String shopName, String mallId, String presidentName, String mallUrl) {
		this.shopNo = shopNo;
		this.shopName = shopName;
		this.mallId = mallId;
		this.presidentName = presidentName;
		this.mallUrl = mallUrl;
	}

	public int getShopNo() {
		return shopNo;
	}

	public void setShopNo(int shopNo) {
		this.shopNo = shopNo;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMallId() {
		return mallId;
	}

	public void setMallId(String mallId) {
		this.mallId = mallId;
	}

	public String getPresidentName() {
		return presidentName;
	}

	public void setPresidentName(String presidentName) {
		this.presidentName = presidentName;
	}

	public String getMallUrl() {
		return mallUrl;
	}

	public void setMallUrl(String mallUrl) {
		this.mallUrl = mallUrl;
	}
	
}
