package com.cafe24.social.oauth2.cafe24.api.impl;

import java.net.URI;

import org.springframework.social.support.URIBuilder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.cafe24.social.oauth2.cafe24.api.Shop;
import com.cafe24.social.oauth2.cafe24.api.ShopOperations;

public class ShopTemplate implements ShopOperations{
	
	/*
	curl -X GET \
  	'https://{mallid}.cafe24api.com/api/v2/admin/store?shop_no=1' \
  	-H 'Authorization: Bearer {access_token}' \
  	-H 'Content-Type: application/json' 
	 */
	
	private final RestTemplate restTemplate;
	private final RestTemplate usingApiRestTemplate;
	
	public ShopTemplate(RestTemplate restTemplate, RestTemplate usingApiRestTemplate) {
		this.restTemplate = restTemplate;
		this.usingApiRestTemplate = usingApiRestTemplate;
	}
	
	// {mallid} - 임시로 지정함. -> AccessGrant에게 얻어야 할 듯.
	public String getBaseCafe24ApiUrl() {
		return "https://qyuee.cafe24api.com";
	}
	
	public Shop getShopInfo() {
		
		System.out.println(" >>> getshopinfo() is called!! ");
		
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("shop_no", "1");
		
		URI apiUrl = buildApiUri("/api/v2/admin/store", parameters);
		
		System.out.println(" >>> apiUrl.getPath() : "+apiUrl.getPath());
		
		return usingApiRestTemplate.getForObject(apiUrl, Shop.class);
	}
	
	protected URI buildApiUri(String path, MultiValueMap<String, String> parameters) {
		return URIBuilder.fromUri(getBaseCafe24ApiUrl() + path).queryParams(parameters).build();
	}

	@Override
	public Shop getShopNo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Shop getShopName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Shop getMallId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Shop getPresidentName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Shop getMallUrl() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
