package com.cafe24.social.oauth2.cafe24.api;

import java.util.HashMap;
import java.util.Map;

/**
 * Cafe24 
 * @author bit
 *
 */
public abstract class Cafe24Object {
	private Map<String, Object> extraData;
	
	public Cafe24Object() {
		this.extraData = new HashMap<String, Object>();
	}
	
	public Map<String, Object> getExtraData() {
		return extraData;
	}
	
	protected void add(String key, Object value) {
		extraData.put(key, value);
	}
}
