package com.cafe24.devbit005;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cafe24.oauth.domain.TokenVo;
import com.cafe24.oauth.service.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class ApiController {
	
	@Autowired
	private TokenService tokenservice;
	
	@RequestMapping(value="/getToken2", method=RequestMethod.POST)
	public void getToken2(
			@RequestParam("code") String code,
			@RequestParam("redirect_uri") String redirectUri,
			RedirectAttributes attributes,
			NativeWebRequest request
			) throws ClientProtocolException, IOException {
		
		String clientId = "PLJ3LsIxIdkkcvwiEjejoH";
		String clientSecret = "WI0AD13hJpugDMUfOMN11H";
		String target = clientId+":"+clientSecret; 
		String authUrl = "https://qyuee.cafe24api.com/api/v2/oauth/token";
		String grantType = "authorization_code";
		//String redirectUri = request.getParameter("redirectUri");
		//String redirectUri = "https://lee33397.cafe24.com/devbit005/oauth/callback";
		
		CloseableHttpClient client = HttpClients.createDefault();
		
		HttpPost httpPost = new HttpPost(authUrl);
		
		httpPost.addHeader("Authorization","Basic "+Base64Utils.encodeToString(target.getBytes()));
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
		System.out.println("\n\n==========================================================");
		Header[] headers = httpPost.getAllHeaders();
		
		for(Header header : headers) {
			System.out.println(header.getName()+" : "+header.getValue());
		}
		
		System.out.println("===========================================================\n\n");
		
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("grant_type", grantType));
		postParameters.add(new BasicNameValuePair("code", code)); 
		postParameters.add(new BasicNameValuePair("redirect_uri", redirectUri));
		
		httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
		
		CloseableHttpResponse httpResponse = client.execute(httpPost);
		
		System.out.println(httpResponse.getStatusLine().getStatusCode());
		
		String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
		
		System.out.println("\n\n======================caf24 api server response==============");
		
		System.out.println("전달한 파라미터");
		
		System.out.println(json);
		
		System.out.println("===============================================================\n\n");
		
		client.close();
		
		/*	
		 * 	cafe24 api 서버로 부터 토큰에 대한 정보를 얻었다.
		 *	이를 /oauth/callback 으로 전달하여 객체화 하고 영속화 할 것이다.
		*/
		
		ObjectMapper mapper = new ObjectMapper();
		
		TokenVo tokenVo = mapper.readValue(json, TokenVo.class);
		
		// 저장
		tokenservice.save(tokenVo);
		
		System.out.println(tokenVo);
		
		/*RedirectView result = new RedirectView(redirectUri);
		
		return result;*/
	}
	
	@RequestMapping(value="/refreshToken", method=RequestMethod.POST)
	public String refreshToken() {
		return "";
	}
	
}
