package com.cafe24.devbit005;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cafe24.oauth.domain.TokenVo;
import com.cafe24.oauth.service.TokenService;

@Controller
@RequestMapping("/admin/token/manager")
public class TokenAdminController {
	
	@Autowired
	private TokenService tokenservice;
	
	@RequestMapping("/alltoken")
	public String getAllTokenList(
			Model model
			) {
		
		List<TokenVo> tokens = tokenservice.alltoken();
		
		model.addAttribute("tokens", tokens);
		
		return "devbit005/token_manager";
	}
	
}
