package com.cafe24.devbit005;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class DevBit005Contorller {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String AppMain(NativeWebRequest request, Model model) {

		Map<String, String> params = new HashMap<String, String>();

		System.out.println("\n===========================================================");

		for (Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
			System.out.print("key:" + entry.getKey() + ", value:");

			for (String val : entry.getValue()) {
				System.out.print(val + "\n");
				params.put(entry.getKey(), val);
			}

		}
		System.out.println("===========================================================\n");
		/*
		 * Map<String, String[]> parameters = request.getParameterMap();
		 * 
		 * Set<?> paramKey = parameters.keySet();
		 * 
		 * Iterator<?> keys = paramKey.iterator();
		 * 
		 * while(keys.hasNext()) { System.out.println(parameters.get(keys.next())); }
		 */

		Set<String> keys = params.keySet();

		Iterator<String> iterator = keys.iterator();

		while (iterator.hasNext()) {
			String key = iterator.next();
			System.out.println("key : " + key);
			System.out.println("value : " + params.get(key));
		}

		model.addAttribute("params", params);
		return "devbit005/AppMain";
	}

	@RequestMapping(value = "/oauth/callback")
	public String oAuthCallBack() {

		System.out.println(View.RESPONSE_STATUS_ATTRIBUTE);
		System.out.println(HttpStatus.TEMPORARY_REDIRECT);

		return "devbit005/token";
	}

	@RequestMapping(value = "/prepareGetCode", method = RequestMethod.GET)
	public String prepareGetCode() {

		return "devbit005/prepareGetCode";
	}

	// 코드 발급
	@RequestMapping(value = "/getCode", method = RequestMethod.GET)
	public RedirectView getCode(
			@RequestParam("mallid") String mallid,
			@RequestParam("scope") List<String> socpeList
			) {

		String clientId = "PLJ3LsIxIdkkcvwiEjejoH";
		String state = "lee";
		String redirectUri = "https://lee33397.cafe24.com/devbit005/codeReceiver";

		for (String value : socpeList) {
			System.out.println(value);
		}

		String scope = String.join(",", socpeList);

		System.out.println(scope);

		return new RedirectView(
				"https://" + mallid + ".cafe24api.com/api/v2/oauth/authorize?response_type=code&client_id=" + clientId
						+ "&state=" + state + "&redirect_uri=" + redirectUri + "&scope=" + scope);
	}

	/**
	 * 리소스 서버로 부터 받은 code정보를 받아 보여주는 컨트롤러
	 * 
	 * Response 형식 HTTP/1.1 302 Found Location:
	 * {redirect_uri}?code={authorize_code}&state={state}
	 * 
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@RequestMapping(value = "/codeReceiver", method = RequestMethod.GET)
	public RedirectView redirectURL(
			NativeWebRequest request,
			Model model,
			@RequestParam("code") String code
			) throws ClientProtocolException, IOException {

		String getTokenUrl = "https://lee33397.cafe24.com/devbit005/getToken2";
		String redirectUri = "https://lee33397.cafe24.com/devbit005/oauth/callback";

		//Map<String, String> params = new HashMap<>();
		
		/*for (Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
			for (String val : entry.getValue()) {
				System.out.print(val + "\n");
				if (entry.getKey() == "code") {
					code = val;
				}
				params.put(entry.getKey(), val);
			}
		}*/

		//model.addAttribute("code", code);
		//model.addAttribute("params", params);
		/* return "devbit005/codeReceiver"; */
		
		CloseableHttpClient client = HttpClients.createDefault();
		
		HttpPost httpPost = new HttpPost(getTokenUrl);
		
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("code", code));
		postParameters.add(new BasicNameValuePair("redirect_uri", redirectUri));
		
		httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
		
		client.execute(httpPost);
		
		client.close();
		
		RedirectView result = new RedirectView(redirectUri);
		
		return result;
	}

	@RequestMapping(value = "/useApi/{mallid}", method = RequestMethod.GET)
	public String useToken(@PathVariable("mallid") String mallid) {

		// cafe24 쇼핑몰 로그인 여부 확인
		// 토큰 발급
		// 토큰이 있다면 DB에 있는거 조회해서 사용가능한지 확인
		// 토큰 없으면 새로 발급 로직 수행
		
		return "devbit005/useApi";
	}

}
