package com.cafe24.lee.apiserver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/api")
public class ApiMainController {
	
	@ResponseBody
	@RequestMapping(value="/getCode", method=RequestMethod.GET)
	public RedirectView getCode(
			) {
		
		String redirectUrl = "http://localhost:8080/devbit005/api/codeReceiver";
		
		Map<String, String> parameters = new HashMap();
		
		// 코드 발급 해주고 넘어온 redirect_url로 리다이렉션 해야한다.
		String code = "testcode";
		String state = "lee";
		
		return new RedirectView(redirectUrl+"?code="+code+"&state="+state);
	}
	
	@ResponseBody
	@RequestMapping(value="/getToken", method=RequestMethod.POST)
	public Map<String, String> getToken(
			
			) {
		Map<String, String> parameters = new HashMap<String, String>();
		
		parameters.put("accessToken", "testcode"+" accesstoekn");
		parameters.put("state", "lee");
		
		return parameters;
	}
	
	@RequestMapping(value="/codeReceiver", method=RequestMethod.GET)
	public String redirectURL(NativeWebRequest request, Model model) {
		String code="";
		
		Map<String, String> params = new HashMap<String, String>();
		
		for (Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
			for(String val : entry.getValue()) {
				System.out.print(val+"\n");
				if(entry.getKey()=="code") {
					code=val;
				}
				
				params.put(entry.getKey(), val);
			}
		}
		
		model.addAttribute("code", code);
		model.addAttribute("params", params);
		return "devbit005/codeReceiver";
	}
	
	@RequestMapping(value="/oauth/token", method=RequestMethod.POST)
	public String token() {
		return "";
	}
	
	@ResponseBody
	@RequestMapping(value="/oauth/callback", method=RequestMethod.GET)
	public String oauthCallBack() {
		
		return "token 발급";
	}
	
	@RequestMapping(value="/getToken2", method=RequestMethod.POST)
	public void getToken2(
			@RequestParam("code") String code
			) throws ClientProtocolException, IOException {
		
		String clientId = "PLJ3LsIxIdkkcvwiEjejoH";
		String clientSecret = "WI0AD13hJpugDMUfOMN11H";
		String target = "{"+clientId+"}:{"+clientSecret+"}";
		String authUrl = "http://localhost:8080/devbit005/api/oauth/token";
		String grantType = "authorization_code";
		String redirectUri = "http://localhost:8080/devbit005/api/oauth/callback";
		
		CloseableHttpClient client = HttpClients.createDefault();
		
		HttpPost httpPost = new HttpPost(authUrl);
		
		httpPost.addHeader("Authorization",Base64Utils.encodeToString(target.getBytes()));
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
		
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("grantType", grantType));
		postParameters.add(new BasicNameValuePair("code", code));
		postParameters.add(new BasicNameValuePair("redirect_uri", redirectUri));
		
		CloseableHttpResponse httpResponse = client.execute(httpPost);
		
		System.out.println(httpResponse.getStatusLine().getStatusCode());
		
		String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
		
		System.out.println(json);
		
		client.close();
	}
	
}
