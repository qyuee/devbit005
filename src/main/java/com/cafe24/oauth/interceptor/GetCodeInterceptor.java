package com.cafe24.oauth.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class GetCodeInterceptor extends HandlerInterceptorAdapter {

	// 인터셉터 전 처리
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		
		String code = request.getParameter("code");
		
		if(code!=null) {
			// 코드를 cafe24api.com/api/v2/oauth/authorize 으로 부터 받아옴
			response.sendRedirect(request.getContextPath()+"/stopPoint1?code="+code);
			return false;
		}
		
		return true; // false이면 인터셉터가 response 해야함. false이기에 인터셉터에서 응답이 막힘.
	}

	// 컨트롤러 실행 이후
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		System.out.println("MyInterceptor:postHandle");
		// TODO Auto-generated method stub

	}

}
