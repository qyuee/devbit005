package com.cafe24.oauth.domain;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
@Table(name="token")
@JsonAutoDetect (
		fieldVisibility = Visibility.NONE,getterVisibility = Visibility.NONE,
		setterVisibility = Visibility.NONE)
@JsonPropertyOrder({"accessToken", "expiresAt", "refreshToken", "clientId", "mallId", "userId", "scopes", "issuedAt"})
public class TokenVo {
	
	@Id
	@Column(name="user_id", nullable=true)
	@JsonProperty("user_id")
	private String userId;
	
	@Column(name="access_token")
	@JsonProperty("access_token")
	private String accessToken;
	
	@Column(name="expires_at")
	@JsonProperty("expires_at")
	private String expiresAt;
	
	@Column(name="refresh_token")
	@JsonProperty("refresh_token")
	private String refreshToken;
	
	@Column(name="client_id")
	@JsonProperty("client_id")
	private String clientId;
	
	@Column(name="mall_id")
	@JsonProperty("mall_id")
	private String mallId;
	
	@Column(name="scopes")
	@JsonProperty("scopes") 
	private String[] scopes;
	
	@Column(name="issued_at")
	@JsonProperty("issued_at")
	private String issuedAt;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(String expiresAt) {
		this.expiresAt = expiresAt;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getMallId() {
		return mallId;
	}

	public void setMallId(String mallId) {
		this.mallId = mallId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String[] getScopes() {
		return scopes;
	}

	public void setScopes(String[] scopes) {
		this.scopes = scopes;
	}

	public String getIssuedAt() {
		return issuedAt;
	}

	public void setIssuedAt(String issuedAt) {
		this.issuedAt = issuedAt;
	}

	@Override
	public String toString() {
		return "TokenVo [accessToken=" + accessToken + ", expiresAt=" + expiresAt + ", refreshToken=" + refreshToken
				+ ", clientId=" + clientId + ", mallId=" + mallId + ", userId=" + userId + ", scopes="
				+ Arrays.toString(scopes) + ", issuedAt=" + issuedAt + "]";
	}
	
}
