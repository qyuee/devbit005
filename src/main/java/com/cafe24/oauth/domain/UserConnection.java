package com.cafe24.oauth.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(UserConnectionPK.class)
public class UserConnection {
	
	@Id
	@Column(name="userId", nullable=false, length=255)
	private String userId;
	
	@Id
	@Column(name="providerId", nullable=false, length=255)
	private String providerId;
	
	@Column(name="providerUserId", nullable=true, length=255)
	private String providerUserId;
	
	@Column(name="rank", nullable=false)
	private int rank;
	
	@Column(name="displayName", nullable=true, length=255)
	private String displayName;
	
	@Column(name="profileUrl", nullable=true, length=512)
	private String profileUrl;
	
	@Column(name="imageUrl", nullable=true, length=512)
	private String imageUrl;
	
	@Column(name="accessToken", nullable=false, length=255)
	private String accessToken;
	
	@Column(name="secret", nullable=true, length=255)
	private String secret;
	
	@Column(name="refreshToken", nullable=true, length=255)
	private String refreshToken;
	
	@Column(name="expireTime")
	private long expireTime;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public String getProviderUserId() {
		return providerUserId;
	}
	public void setProviderUserId(String providerUserId) {
		this.providerUserId = providerUserId;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getProfileUrl() {
		return profileUrl;
	}
	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public long getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}
	
	@Override
	public String toString() {
		return "UserConnection [userId=" + userId + ", providerId=" + providerId + ", providerUserId=" + providerUserId
				+ ", rank=" + rank + ", displayName=" + displayName + ", profileUrl=" + profileUrl + ", imageUrl="
				+ imageUrl + ", accessToken=" + accessToken + ", secret=" + secret + ", refreshToken=" + refreshToken
				+ ", expireTime=" + expireTime + "]";
	}
	
}
