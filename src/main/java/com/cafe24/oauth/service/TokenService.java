package com.cafe24.oauth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cafe24.oauth.domain.TokenVo;
import com.cafe24.oauth.repository.TokenRepository;

@Service
public class TokenService {
	@Autowired
	private TokenRepository tokenrepository;
	
	public void save(TokenVo tokenVo) {
		tokenrepository.save(tokenVo);
	}
	
	public TokenVo show() {
		return tokenrepository.findOne("qyuee");
	}
	
	public List<TokenVo> alltoken(){
		return tokenrepository.findAll();
	}
	
}
