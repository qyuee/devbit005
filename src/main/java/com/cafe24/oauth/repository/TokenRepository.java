package com.cafe24.oauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cafe24.oauth.domain.TokenVo;

@Repository
public interface TokenRepository extends JpaRepository<TokenVo, String>{
	
}
