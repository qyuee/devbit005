<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>코드 발급 전 단계</h1>
	
	<form action="${pageContext.servletContext.contextPath }/getCode" method="get"> 
		<label>접속할 mallid:</label>
		<input type="text" name="mallid" placeholder="mallid"><br>
		
		<label>scope 선택</label><br>
		<span><input type="checkbox" name="scope" value="mall.write_category">상품분류 쓰기권한<br></span>
		<span><input type="checkbox" name="scope" value="mall.read_category">상품분류 읽기권한<br></span>
		
		<span><input type="checkbox" name="scope" value="mall.write_product">상품 쓰기 권한<br></span>
		<span><input type="checkbox" name="scope" value="mall.read_product">상품 읽기 권한<br></span>
		
		<input type="submit" name="getCode-btn" value="코드 발급 가즈아">
	</form>
</body>
</html>