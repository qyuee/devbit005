<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>App Main - Jenkins 연동 성공!</h1>
	
	<c:forEach items="${params }" var="entry" varStatus="status">
		Key: <c:out value="${entry.key}"/>
    	Value: <c:out value="${entry.value}"/><br>
	</c:forEach>
	
	
	<form action="${pageContext.servletContext.contextPath }/prepareGetCode" method="get">
		<input type="submit" value="토큰 받으러 가즈아">
	</form>
	
	<form action="${pageContext.servletContext.contextPath }/admin/token/manager/alltoken" method="get">
		<input type="submit" value="토큰 관리자">
	</form>
	
</body>
</html>