<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.td {
	border: 1px solid #000;
}

.table1 {
	border-collapse: collapse;
}
</style>
<title>Insert title here</title>
</head>
<body>
	<h1>Token Manager</h1>

	<table>
		<thead>
			<tr>
				<th>user_id</th>
				<th>access_token</th>
				<th>client_id</th>
				<th>expires_at</th>
				<th>issued_at</th>
				<th>mall_id</th>
				<th>refresh_token</th>
				<th>scopes</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${tokens }" var="token" varStatus="status">
				<tr>
					<td>${token.userId }</td>
					<td>${token.accessToken }</td>
					<td>${token.clientId }</td>
					<td>${token.expiresAt }</td>
					<td>${token.issuedAt }</td>
					<td>${token.mallId }</td>
					<td>${token.refreshToken }</td>
					<td>${token.scopes }</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
</body>
</html>